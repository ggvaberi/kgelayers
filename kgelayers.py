#!/usr/bin/env python

# Layers export in GIMP Python

from gimpfu import *
import gimpfu
import os
import gtk
from array import *

def export_layers(img, draw, path, name):
  #img = img.duplicate()

  for l in img.layers:
    l.visible = False

  text = ""

  for id, l in enumerate(img.layers):
    l.visible = True
    #fname = name % [ id, l.name ]
    fname = "layer_" + str(id) + "_" + l.name + ".png"
    fpath = os.path.join(path, fname)

    #print('Layer name: ' + str(l.name))
    #print('Layer offsets: ' + str(l.offsets))
    #print('Layer resolution: ' + str(l.width) + " " + str(l.height))

    text += ('[' + l.name + '][' + fname + '][' + str(l.offsets[0]) + '][' + str(l.offsets[1]) + '][' + str(l.width) + '][' + str(l.height) + ']' + '\n')
    limg = l.image

    pdb.gimp_file_save(limg, limg.layers[id], fpath, fname)
    l.visible = False

    limg = None
  
  for l in img.layers:
    l.visible = True

  mpath = os.path.join(path, 'level.map')
  fmap = open(mpath, "w");
  fmap.write(text)
  fmap.close()

register(
  "python_fu_export_layers",
  "Export Layers",
  "Exports each layer as a separate file",
  "G@G",
  "",
  "",
  "E_xport Layers...",
  "*",
  [
    (PF_IMAGE, "img", "Input image", None),
    (PF_DRAWABLE, "draw", "Input drawable", None),
    (PF_DIRNAME, "path", "Output directory", os.getcwd()),
    (PF_STRING, "name", "Output name", "layer_%d_%s.png")
  ],
  [],
  export_layers,
  menu="<Image>/File/"
)

main()
